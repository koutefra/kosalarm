﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace KOSalarm
{
    class Parallel
    {
        public int Capacity { get; private set; }
        public bool? CapacityOverfill { get; private set; }
        public int Code { get; private set; }
        public string CourseName { get; private set; }
        public bool IsOpen { get; set; }
        public int Occupied { get; private set; }
        public string ParallelType { get; private set; }
        public string Semester { get; private set; }
        public string Teacher { get; private set; }
        public TimetableSlot TimeData { get; private set; }

        public Parallel(int capacity, bool? capacityOverfill, int code, string courseName, bool isOpen, int occupied,
                        string parallelType, string semester, string teacher, TimetableSlot timeData)
        {
            Capacity = capacity;
            CapacityOverfill = capacityOverfill;
            Code = code;
            CourseName = courseName;
            IsOpen = isOpen;
            Occupied = occupied;
            ParallelType = parallelType;
            Semester = semester;
            Teacher = teacher;
            TimeData = timeData;
        }

        public class TimetableSlot
        {
            public int Day { get; private set; }
            public int FirstHour { get; private set; }
            public int Duration { get; private set; }
            public string Parity { get; private set; }
            public string Room { get; private set; }

            public TimetableSlot(int day, int firstHour, int duration, string parity, string room)
            {
                Day = day;
                FirstHour = firstHour;
                Duration = duration;
                Parity = parity;
                Room = room;
            }

            public override string ToString()
            {
                return GetDayAbb(Day) + " " + FirstHour + "+" + (FirstHour + Duration);
            }

            private string GetDayAbb(int day)
            {
                switch (day)
                {
                    case 1: return "PO";
                    case 2: return "UT";
                    case 3: return "ST";
                    case 4: return "CT";
                    case 5: return "PA";
                    case 6: return "SO";
                    case 7: return "NE";
                }

                throw new Exception();
            }
        }
    }
}
