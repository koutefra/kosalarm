﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace KOSalarm
{
    class ApiClient
    {
        private const string apiServerUri = "https://kosapi.fit.cvut.cz";
        private const string authServerUri = "https://auth.fit.cvut.cz/oauth/token";
        // base-64 coded oauth 2.0 client credentials
        private const string credentials = "Basic MmMwYmY4YTMtMDdmYS00M2Q4LThmOGYtNDg2ZTE0NTIzNWY5OmIwa01Tb3RKYVVOYkR4MVgxcHBqZGJ4cXJhV1MxNUJ2";

        private string accessToken;

        private readonly RestClient client;

        public ApiClient()
        {
            accessToken = GetAccesToken();
            client = new RestClient(apiServerUri);

            RestRequest request = new RestRequest("/api/3/parallels/949882880405", Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + accessToken), ParameterType.HttpHeader);

            Console.WriteLine("SCHEDULING SEMESTER REQUEST (HTTP GET)");

            IRestResponse response = client.Execute(request);
            XDocument doc = XDocument.Parse(response.Content);
            string jsonText = JsonConvert.SerializeXNode(doc);
            // internal location of semester data in received object
            dynamic content = JObject.Parse(jsonText)["atom:entry"]["atom:content"];
            string semester = content.code["#text"];
        }

        public Parallel[] GetCourseParallels(string courseCode, string semesterCode)
        {
            RestRequest request = new RestRequest("/api/3/courses/" + courseCode + "/parallels?sem=" + semesterCode + "&limit=100", Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + accessToken), ParameterType.HttpHeader);

            Console.WriteLine("PARALLELS OF COURSE " + courseCode + " REQUEST (HTTP GET)");

            IRestResponse response = client.Execute(request);
            if (!response.IsSuccessful)
            {
                Console.WriteLine("FAILED");
                Environment.Exit(0);
            }

            XDocument doc = XDocument.Parse(response.Content);
            string jsonText = JsonConvert.SerializeXNode(doc);
            // internal location of current data in received object
            dynamic content = JObject.Parse(jsonText)["atom:feed"]["atom:entry"];

            if (content == null)
            {
                Console.WriteLine("NO CONTENT\\NO PARALLELS");
                return new Parallel[0];
            }

            var parallelCount = content.Count == null ? 1 : content.Count;
            Parallel[] parallels = new Parallel[parallelCount];
            // parse all parallels
            for (int i = 0; i < parallelCount; i++)
            {
                dynamic curParallelContent;
                if (parallelCount == 1)
                    curParallelContent = content["atom:content"];
                else
                    curParallelContent = content[i]["atom:content"];

                var timeData = curParallelContent["timetableSlot"];

                parallels[i] = new Parallel((int)makeNullIfJsonDataMissing(curParallelContent, "capacity", "#text"),
                                            makeNullIfJsonDataMissing(curParallelContent, "capacityOverfill", "#text") == "ALLOWED" ? true : false,
                                            (int)makeNullIfJsonDataMissing(curParallelContent, "code", "#text"),
                                            (string)makeNullIfJsonDataMissing(curParallelContent, "course", "#text"),
                                            makeNullIfJsonDataMissing(curParallelContent, "enrollment", "#text") == "ALLOWED" ? true : false,
                                            (int)makeNullIfJsonDataMissing(curParallelContent,"occupied", "#text"),
                                            (string)makeNullIfJsonDataMissing(curParallelContent, "parallelType", "#text"),
                                            (string)makeNullIfJsonDataMissing(curParallelContent, "semester", "#text"),
                                            (string)makeNullIfJsonDataMissing(curParallelContent, "teacher", "#text"),
                                            new Parallel.TimetableSlot((int)timeData.day,
                                                                       (int)timeData.firstHour,
                                                                       (int)timeData.duration,
                                                                       (string)timeData.parity,
                                                                       (string)timeData.room["#text"]));
            }

            Console.WriteLine("OK");

            return parallels;
        }

        public string GetSemester()
        {
            RestRequest request = new RestRequest("/api/3/semesters/scheduling", Method.GET);
            request.AddParameter("Authorization", string.Format("Bearer " + accessToken), ParameterType.HttpHeader);

            Console.WriteLine("SCHEDULING SEMESTER REQUEST (HTTP GET)");

            IRestResponse response = client.Execute(request);
            if (!response.IsSuccessful)
            {
                Console.WriteLine("FAILED");
                Environment.Exit(0);
            }

            XDocument doc = XDocument.Parse(response.Content);
            string jsonText = JsonConvert.SerializeXNode(doc);
            // internal location of semester data in received object
            dynamic content = JObject.Parse(jsonText)["atom:entry"]["atom:content"];
            string semester = content.code["#text"];

            Console.WriteLine("OK, SEMESTER:" + semester);

            return semester;
        }

        private string GetAccesToken()
        {
            var client = new RestClient(authServerUri);
            var request = new RestRequest(Method.POST);
            request.AddHeader("Authorization", credentials);
            request.AddParameter("grant_type", "client_credentials");

            Console.WriteLine("ACCESS TOKEN REQUEST (HTTP GET)");

            IRestResponse response = client.Execute(request);
            if (!response.IsSuccessful)
            {
                Console.WriteLine("FAILED");
                Environment.Exit(0);
            }

            dynamic parsedContent = JsonConvert.DeserializeObject(response.Content);
            string accessToken = parsedContent.access_token;

            Console.WriteLine("OK, TOKEN:" + accessToken);

            return accessToken;
        }

        /// <summary>
        /// Handle if dynamic data have not expected format.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="jsonAccessCode1"></param>
        /// <param name="jsonAccessCode2"></param>
        /// <returns></returns>
        private dynamic makeNullIfJsonDataMissing(dynamic data, string jsonAccessCode1, string jsonAccessCode2)
        {
            dynamic result;
            try
            {
                result = data[jsonAccessCode1][jsonAccessCode2];
            }
            catch
            {
                result = null;
            }
            return result;
        }
    }
}
