﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace KOSalarm
{
    class App
    {
        private const int pauseTimeMilis = 12000; // 12 sec.

        private readonly string username = ConfigurationManager.AppSettings.Get("KOSusername");
        private readonly string email = ConfigurationManager.AppSettings.Get("Email");
        private readonly string[] coursesCodes = ConfigurationManager.AppSettings.Get("Courses").Split(",");

        private readonly ApiClient apiClient;
        private readonly Course[] courses;

        private readonly string curSemester;

        public App()
        {
            courses = new Course[coursesCodes.Length];
            apiClient = new ApiClient();
            curSemester = apiClient.GetSemester();

            // loads all parallels to all course
            for (int i = 0; i < coursesCodes.Length; i++)
            {
                courses[i] = new Course(coursesCodes[i]);

                // adding parallels
                var allParallels = apiClient.GetCourseParallels(courses[i].Code, curSemester);
                foreach (Parallel parallel in allParallels)
                        courses[i].Parallels.Add(parallel);
            }
        }

        public void Loop()
        {
            int loopNumber = 0;
            while (true)
            {
                Console.WriteLine("REFRESHING DATA (LOOP NUMBER: " + loopNumber++ + ")");
                foreach (Course course in courses)
                {
                    var refreshedParallels = apiClient.GetCourseParallels(course.Code, curSemester);

                    Parallel newOpenedParallel;
                    while ((newOpenedParallel = GetNewOpenedParallel(course, refreshedParallels)) != null)
                        SendEmail(newOpenedParallel);
                }

                System.Threading.Thread.Sleep(pauseTimeMilis);
            }
        }

        private void SendEmail(Parallel newParallel)
        {
            MailAddress from = new MailAddress("KOSapi2018@gmail.com", "KOSalarm");
            MailAddress to = new MailAddress(email);
            MailMessage mail = new MailMessage(from, to);

            mail.Subject = "Paralelka otevrena";
            mail.Body = "Otevrena paralelka z predmetu " + newParallel.CourseName + " (" + newParallel.TimeData.ToString() + ")\n"
                        + "https://kos.is.cvut.cz";

            SmtpClient smtp = new SmtpClient();
            smtp.EnableSsl = true;
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 25;

            smtp.Credentials = new NetworkCredential("KOSapi2018@gmail.com", "Aabbccdd11");

            Console.WriteLine("SENDING EMAIL");
            smtp.Send(mail);
            Console.WriteLine("OK");
        }

        /// <summary>
        /// Checks if there is a missmatch between old parallels and new parallels. If yes, 
        /// changes old parallel to new parallel and returns it.
        /// </summary>
        /// <param name="course"></param>
        /// <param name="refreshedParallels"></param>
        /// <returns></returns>
        private Parallel GetNewOpenedParallel(Course course, Parallel[] refreshedParallels)
        {
            foreach (Parallel refreshedParallel in refreshedParallels)
            {
                var notRefreshedParallel = course.GetParallelByCode(refreshedParallel.Code);

                if (refreshedParallel.IsOpen == true && notRefreshedParallel.IsOpen == false)
                {
                    course.SetParallelByCode(refreshedParallel.Code, refreshedParallel);
                    return refreshedParallel;
                }
            }

            return null;
        }

        private class Course
        {
            public string Code { get; private set; }

            public List<Parallel> Parallels { get; set; }

            public Course(string code)
            {
                Code = code;

                Parallels = new List<Parallel>();
            }

            public Parallel GetParallelByCode(int code)
            {
                foreach (Parallel parallel in Parallels)
                {
                    if (parallel.Code == code)
                        return parallel;
                }

                return null;
            }

            public bool SetParallelByCode(int code, Parallel newParallel)
            {
                for (int i = 0; i < Parallels.Count; i++)
                {
                    if (Parallels[i].Code == code)
                    {
                        Parallels[i] = newParallel;
                        return true;
                    }
                }

                return false;
            }
        }
    }
}
